<!DOCTYPE html>
<html lang="zxx">
<!-- Mirrored from code-theme.com/html/findhouses/index-9.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 May 2023 16:28:22 GMT -->

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="description" content="html 5 template" />
  <meta name="author" content="" />
  <title>Find Houses - HTML5 Template</title>
  <!-- FAVICON -->
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <link rel="stylesheet" href="css/jquery-ui.css" />
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i%7CMontserrat:600,800" rel="stylesheet" />
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="font/flaticon.css" />
  <link rel="stylesheet" href="css/fontawesome-all.min.css" />
  <link rel="stylesheet" href="css/fontawesome-5-all.min.css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" />
  <!-- LEAFLET MAP -->
  <link rel="stylesheet" href="css/leaflet.css" />
  <link rel="stylesheet" href="css/leaflet-gesture-handling.min.css" />
  <link rel="stylesheet" href="css/leaflet.markercluster.css" />
  <link rel="stylesheet" href="css/leaflet.markercluster.default.css" />
  <!-- ARCHIVES CSS -->
  <link rel="stylesheet" href="css/search.css" />
  <link rel="stylesheet" href="css/animate.css" />
  <link rel="stylesheet" href="css/aos.css" />
  <link rel="stylesheet" href="css/aos2.css" />
  <link rel="stylesheet" href="css/magnific-popup.css" />
  <link rel="stylesheet" href="css/lightcase.css" />
  <link rel="stylesheet" href="css/owl.carousel.min.css" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/menu.css" />
  <link rel="stylesheet" href="css/slick.css" />
  <link rel="stylesheet" href="css/styles.css" />
  <link rel="stylesheet" href="css/maps.css" />
  <link rel="stylesheet" id="color" href="css/colors/pink.css" />
</head>

<body class="homepage-9 hd-white mh">
  <!-- Wrapper -->
  <div id="wrapper">
    <!-- START SECTION HEADINGS -->
    <!-- Header Container
        ================================================== -->

    <div class="clearfix"></div>
    <!-- Header Container / End -->

    <!-- STAR HEADER GOOGLE MAP -->

    <!-- START SECTION FEATURED PROPERTIES -->
    <section class="featured portfolio bg-white-2">
      <div class="container-fluid">
        <div class="sec-title">
          <h2>Propiedades</h2>
        </div>
        <div class="row portfolio-items">


          <?php
          $url = 'http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml ';
          $xml = file_get_contents($url);
          $data = simplexml_load_string($xml);

          foreach ($data->propiedad as $propiedad) {
            $id = (string) $propiedad->id;
            $titulo1 = (string) $propiedad->titulo1;
            $foto1 = (string) $propiedad->foto1;
            $accion = (string) $propiedad->accion;
            $ciudad = (string) $propiedad->ciudad;
            $precioinmo = (string) $propiedad->precioinmo;




            ?>

            <div class="item col-lg-3 col-md-6 col-xs-12 landscapes sale">
              <div class="project-single" data-aos="fade-up" data-aos-delay="150">
                <div class="project-inner project-head">
                  <div class="homes">
                    <!-- homes img -->
                    <a href="single-property-1.html" class="homes-img">

                      <div class="homes-tag button alt sale">
                        <?= $accion ?>
                      </div>
                      <div class="homes-price">
                        <?= $precioinmo ?>€
                      </div>
                      <img src="<?= $foto1 ?>" alt="home-1" class="img-responsive" />
                    </a>
                  </div>
                  <div class="button-effect">

                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" class="btn popup-video popup-youtube"><i
                        class="fas fa-video"></i></a>
                    <a href="propiedad.php?id=<?= $id ?>" class="img-poppu btn"><i class="fa fa-photo"></i></a>
                  </div>
                </div>
                <!-- homes content -->
                <div class="homes-content">
                  <!-- homes address -->
                  <h3>
                    <a href="single-property-1.html">
                      <?= $titulo1 ?>
                    </a>
                  </h3>
                  <p class="homes-address mb-3">
                    <a href="single-property-1.html">
                      <i class="fa fa-map-marker"></i><span>
                        <?= $ciudad ?>
                      </span>
                    </a>
                  </p>
                  <!-- homes List -->
                  <ul class="homes-list clearfix pb-3">
                    <li class="the-icons">
                      <i class="flaticon-bed mr-2" aria-hidden="true"></i>
                      <span>6 Habitaciones</span>
                    </li>
                    <li class="the-icons">
                      <i class="flaticon-bathtub mr-2" aria-hidden="true"></i>
                      <span>3 Baños</span>
                    </li>
                    <li class="the-icons">
                      <i class="flaticon-square mr-2" aria-hidden="true"></i>
                      <span>720 m2</span>
                    </li>
                    <li class="the-icons">
                      <i class="flaticon-car mr-2" aria-hidden="true"></i>
                      <span>2 Garajes</span>
                    </li>
                  </ul>

                </div>
              </div>
            </div>



            <?php
          }
          ?>













        </div>
      </div>
  </div>

  </div>
  </section>
  <!-- END SECTION FEATURED PROPERTIES -->



  <!-- ARCHIVES JS -->
  <script src="js/jquery-3.5.1.min.js"></script>
  <script src="js/rangeSlider.js"></script>
  <script src="js/tether.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/moment.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/mmenu.min.js"></script>
  <script src="js/mmenu.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/aos2.js"></script>
  <script src="js/slick.min.js"></script>
  <script src="js/fitvids.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.counterup.min.js"></script>
  <script src="js/imagesloaded.pkgd.min.js"></script>
  <script src="js/isotope.pkgd.min.js"></script>
  <script src="js/smooth-scroll.min.js"></script>
  <script src="js/lightcase.js"></script>
  <script src="js/search.js"></script>
  <script src="js/owl.carousel.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/ajaxchimp.min.js"></script>
  <script src="js/newsletter.js"></script>
  <script src="js/jquery.form.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <script src="js/searched.js"></script>
  <script src="js/forms-2.js"></script>
  <script src="js/leaflet.js"></script>
  <script src="js/leaflet-gesture-handling.min.js"></script>
  <script src="js/leaflet-providers.js"></script>
  <script src="js/leaflet.markercluster.js"></script>
  <script src="js/map-style2.js"></script>
  <script src="js/range.js"></script>
  <script src="js/color-switcher.js"></script>

  <!-- Slider Revolution scripts -->
  <script src="revolution/js/jquery.themepunch.tools.min.js"></script>
  <script src="revolution/js/jquery.themepunch.revolution.min.js"></script>

  <script>
    $(".slick-lancers").slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 1292,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 993,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
          },
        },
      ],
    });
  </script>

  <script>
    $(".dropdown-filter").on("click", function () {
      $(".explore__form-checkbox-list").toggleClass("filter-block");
    });
  </script>

  <!-- MAIN JS -->
  <script src="js/script.js"></script>
  </div>
  <!-- Wrapper / End -->
</body>

<!-- Mirrored from code-theme.com/html/findhouses/index-9.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 May 2023 16:28:25 GMT -->

</html>