<?php
$url = 'http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml';
$xml = file_get_contents($url);
$data = simplexml_load_string($xml);





if (!isset($_GET['id'])) {
    echo 'No ID specified.';
    exit();
}

$id = $_GET['id'];

// Retrieve the specific element based on the ID
$element = null;
foreach ($data->propiedad as $propiedad) {
    if ((string) $propiedad->id === $id) {
        $element = $propiedad;
        break;
    }
}

if (!$element) {
    echo 'Element not found.';
    exit();
}

// Display the details of the selected element
echo 'ID: ' . $element->id . '<br>';
echo 'Titulo1: ' . $element->titulo1 . '<br>';
// Display other details as needed
?>